# import 2018 refuge data

library(foreign)
nvs2018 <-
  read.spss(
    "C:/Users/klyon/hd_visitorsurvey/nvs-reports/data/NVSSampleData2018.sav",
    use.value.labels = TRUE,
    to.data.frame = TRUE,
    stringsAsFactors = FALSE
  )


# create age categories
attach(nvs2018)
nvs2018$AGECAT[AGE < 34] <- "18-34 years"
nvs2018$AGECAT[AGE >= 35 & AGE <= 49] <- "35-49 years"
nvs2018$AGECAT[AGE >= 50 & AGE <= 64] <- "50-64 years"
nvs2018$AGECAT[AGE >= 65] <- "65+ years"
detach(nvs2018)
table(nvs2018$AGECAT)



# save data as csv file
write.csv(nvs2018,
          file = "data/nvs2018.csv",
          row.names = FALSE)

nvs2018 <- read.csv("C:/Users/klyon/hd_visitorsurvey/nvs-reports/data/nvs2018.csv")
